let human = [];
human[0] = {
  name: "Mark",
  age: 19,
  city: ["თბილისი", "ლონდონი", "რომი", "ბერლინი"],
  cash: [120, 200, 150, 140],
};
human[1] = {
  name: "Bob",
  age: 21,
  city: ["მაიამი", "მოსკოვი", "ვენა", "რიგა", "კიევი"],
  cash: [90, 240, 100, 76, 123],
};
human[2] = {
  name: "Sam",
  age: 22,
  city: ["თბილისი", "ბუდაპეშტი", "ვარშავა", "ვილნიუსი"],
  cash: [118, 95, 210, 236],
};
human[3] = {
  name: " Anna",
  age: 20,
  city: ["ნიუ", "იორკი", "ათენი", "სიდნეი", "ტოკიო"],
  cash: [100, 240, 50, 190],
};
human[4] = {
  name: "Alex",
  age: 23,
  city: ["პარიზი", "თბილისი", "მადრიდი", "მარსელი", "მინსკი"],
  cash: [96, 134, 76, 210, 158],
};

// 21 +
for (let i = 0; i < human.length; i++) {
  human[i].age >= 21 ? (human[i].over21 = true) : (human[i].over21 = false);
}

// visited/not visited Tbilisi

for (let i = 0; i < human.length; i++) {
  human[i].Tbilisi = "not visited";
  for (let j = 0; j < human[i].city.length; j++)
    if (human[i].city[j] == "თბილისი") {
      human[i].Tbilisi = "visited";
    }
}
// cash Spend
for (let i = 0; i < human.length; i++) {
  human[i].cashSpent;
  let sum = 0;
  for (let j = 0; j < human[i].cash.length; j++) {
    sum += human[i].cash[j];
  }
  human[i].cashSpent = sum;
}
console.log(human);

// cash art ave
let cashSpentSum = 0;
for (let i = 0; i < human.length; i++) {
  cashSpentSum += human[i].cashSpent;
}
cashSpentSum /= human.length;
console.log(cashSpentSum);

// max spend
let maxspent = 0;
let maxSpentName = 0;

for (let i = 0; i < human.length; i++) {
  if (human[i].cashSpent > maxspent) {
    maxspent = human[i].cashSpent;
    maxSpentName = human[i].name;
  }
}
console.log(` ${maxSpentName} has ${maxspent} scores`);
